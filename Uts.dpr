program Uts;

uses
  Forms,
  Menu_report_user in 'Menu_report_user.pas' {form_data_user},
  formretur in 'formretur.pas' {form_retur},
  form_input_stock_barang in 'form_input_stock_barang.pas' {form_input_stock},
  form_input_master_barang in 'form_input_master_barang.pas' {form_input_barang},
  Stok_barang in 'Stok_barang.pas' {form_data_stok},
  Data_user in 'Data_user.pas' {Form4};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(Tform_data_stok, form_data_stok);
  Application.CreateForm(Tform_data_user, form_data_user);
  Application.CreateForm(Tform_retur, form_retur);
  Application.CreateForm(Tform_input_barang, form_input_barang);
  Application.CreateForm(Tform_input_stock, form_input_stock);
  Application.Run;
end.
