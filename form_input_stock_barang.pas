unit form_input_stock_barang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  Tform_input_stock = class(TForm)
    Label1: TLabel;
    StaticText1: TStaticText;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    EJumlah: TEdit;
    BSave: TBitBtn;
    CBKodeBarang: TComboBox;
    BClose: TBitBtn;
    Label4: TLabel;
    ENamaBarang: TEdit;
    Label5: TLabel;
    EBrand: TEdit;
    Label6: TLabel;
    EJenisBarang: TEdit;
    Label7: TLabel;
    EUkuran: TEdit;
    Label8: TLabel;
    EWarna: TEdit;
    Label9: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_input_stock: Tform_input_stock;

implementation

{$R *.dfm}

end.
