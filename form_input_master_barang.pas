unit form_input_master_barang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  Tform_input_barang = class(TForm)
    Label1: TLabel;
    StaticText1: TStaticText;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EJumlah: TEdit;
    BSave: TBitBtn;
    BClose: TBitBtn;
    ENamaBarang: TEdit;
    EBrand: TEdit;
    EJenisBarang: TEdit;
    EUkuran: TEdit;
    EWarna: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_input_barang: Tform_input_barang;

implementation

{$R *.dfm}

end.
